<?php

namespace App\Http\Controllers;

use App\Service\UserInterface;
use App\Service\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public $userService;
    public function __construct(UserInterface $userService) // object instance UserService
    {
        $this->userService = $userService;
    }

    public function profile(Request $request)
    {
        $email = $request->input("email", "");
        $password = $request->input("password", "");
        $user = $this->userService->login($email, $password);
        if ($user)
            return "Hello $user->name <br/>";
        return "Tài khoản và mật khẩu không chính xác!";
    }
}
