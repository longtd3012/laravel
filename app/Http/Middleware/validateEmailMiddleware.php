<?php

namespace App\Http\Middleware;

use App\Utils\Utils;
use Closure;

class validateEmailMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = $request->input("email", "");
        if (Utils::ValidatedEmail($email)) {
            return $next($request);
        }
        dd("Email không hợp lệ");
    }
}
