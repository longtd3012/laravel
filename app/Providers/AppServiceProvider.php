<?php

namespace App\Providers;

use App\Service\BankPaymentService;
use App\Service\CreditPaymentService;
use App\Service\PaymentInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // bind
//        $this->app->bind(BankPaymentService::class, function($app) {
//            return new BankPaymentService('usd'); // reslove
//        });
        $this->app->singleton(PaymentInterface::class, function ($app) {
            if (request()->has("credit"))
                return new CreditPaymentService('usd'); // reslove
            return new BankPaymentService('usd'); // reslove

        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
