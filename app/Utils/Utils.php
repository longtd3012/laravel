<?php

namespace App\Utils;

class Utils
{
    public static function ValidatedEmail($email)
    {
        $regx = "/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix";
        return preg_match($regx, $email);
    }
}
