<?php


namespace App\Service;


use Illuminate\Support\Str;

class BankPaymentService implements PaymentInterface
{
    private $currency;
    private $discount;

    public function __construct($currency)
    {
        $this->currency = $currency;
        $this->discount = 0;
    }

    public function charge($amount)
    {
        return [
            'amount' => $amount - $this->discount,
            'trans_id' => Str::random(),
            'currency' => $this->currency,
            'discount' => $this->discount
        ];
    }

    public function setDiscount($amount)
    {
        $this->discount = $amount;
    }
}
