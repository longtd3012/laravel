<?php


namespace App\Service;


class OrderDetailService
{
    /**
     * @var paymentService
     */
    private $paymentService;

    public function __construct(PaymentInterface $payment)
    {
        $this->paymentService = $payment;
    }

    public function all()
    {
        $this->paymentService->setDiscount(20);
        return [
            'name' => 'Long'
        ];
    }
}
