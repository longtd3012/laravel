<?php

namespace App\Service;

interface UserInterface
{
    public function getAll();
    public function findByEmail($email);
    public function login($email, $password);
}
