<?php

namespace App\Service;

use App\User;

class UserService implements UserInterface
{
    public function login($email, $password)
    {
        $user = User::where('email', '=', $email)->where('password', '=', $password)->first();
        return $user;
    }

    public function getAll()
    {
        return User::findAll();
    }

    public function findByEmail($email)
    {
        // TODO: Implement findByEmail() method.
    }
}
