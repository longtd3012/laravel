<?php

namespace App\Service;

interface PaymentInterface
{
    public function charge($amount);

    public function setDiscount($amount);
}
