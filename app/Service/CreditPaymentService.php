<?php


namespace App\Service;


use Illuminate\Support\Str;

class CreditPaymentService implements PaymentInterface
{
    private $currency;
    private $discount;

    public function __construct($currency)
    {
        $this->currency = $currency;
        $this->discount = 0;
    }

    public function charge($amount)
    {
        $fees = $amount * 0.3;
        return [
            'amount' => ($amount - $this->discount) + $fees,
            'trans_id' => Str::random(),
            'currency' => $this->currency,
            'discount' => $this->discount,
            'fees' => $fees
        ];
    }

    public function setDiscount($amount)
    {
        $this->discount = $amount;
    }
}
