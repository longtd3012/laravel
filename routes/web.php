<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\validateEmailMiddleware;

Route::get('/', function () {
    return view('welcome');
})->name("home.index");

Route::get('/redirect', function () {
    return redirect()->route("home.index");
});

Route::get('/param/{id?}', function ($id = 0) {
    echo 'Param: ' . $id;
})->where('id', '[0-9]+'); // Where : regular ex

// Group prefix
Route::group(['prefix' => 'admin'], function () {
    Route::get('users/{id}', function ($id) {
        echo "Hello $id";
    })->where('id', '[0-9]+');

    Route::get('users/{name?}', function ($name = "Longtd") {
        echo "Hello $name";
    })->where('name', '[A-Za-z]+');
});

// Router with controller.
Route::get('users/profile', 'UserController@profile')->middleware('email');

Route::get('/pay', 'PaymentController@pay');
