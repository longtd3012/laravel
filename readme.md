## Demo Laravel version 5.8.*
Required OS
- PHP > 7.2
- Apache/2.4.*
- Mysql > 5.*

Run project
- php artisan migrate
- php artisan db:seed
- php artisan serve

## Routes
- basic : / 
- with param : /param/id?
- with prefix : /admin/users/id? , /admin/users/name?

## Middleware
Validate email
- /users/profile?email=&password=

## Service provider 
Init payment system : bank or credit
- /pay
- /pay?credit=

## refer
- [laravel.com](https://laravel.com/)
- [viblo.asia](https://viblo.asia/)
- [youtube.com](https://youtube.com/)
- [stackoverflow.com](https://stackoverflow.com/)
## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
