<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \App\User::insert([
            ['name' => 'Longtd', 'email' => 'longtd@funtap.vn', 'password' => '123456'],
            ['name' => 'Sontt', 'email' => 'sontt@funtap.vn', 'password' => '123456']
        ]);
    }
}
